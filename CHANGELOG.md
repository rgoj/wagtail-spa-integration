# 1.1.0

- Added support for drafts when a user edits a slug field by searching revision data for slugs
